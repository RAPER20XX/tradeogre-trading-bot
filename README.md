# TradeOgre Trading Bot
Based off of https://github.com/JonPizza/trade-ogre-trader, https://github.com/JasonRBowling/cryptoTradingBot, and https://github.com/freqtrade/freqtrade

Remember, I am not liable if you lose money, use this program at your own risk as with any trading bot.

## TODO:
[The TODO list](/TODO.md)

## How to use:
1. `git clone https://gitlab.com/Titaniumtown/tradeogre-trading-bot`
2. `cd tradeogre-trading-bot`
3. Download and install required dependencies: `pip install --user -r requirements.txt`
4. Run to initialize config file: `python trader.py`
5. Configure your tokens and settings: `nano config.yml`
6. Run the bot! `python trader.py`
