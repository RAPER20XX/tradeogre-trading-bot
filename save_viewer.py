import pickle
import pandas as pd
import os.path as path
from os.path import isfile
from libs.coin import coin
import matplotlib.pyplot as plt

class SaveViewer:
    coinState = []
    data = pd.DataFrame()

    def __init__(self):
        if path.exists("coinstate.pickle"):
            # load state
            print("Saved state found, loading.")
            with open('coinstate.pickle', 'rb') as f:
                self.coinState = pickle.load(f)
        
        if path.exists("data.pickle"):
            print("Restoring state...")

            self.data = pd.read_pickle('data.pickle')
            # print(self.data.tail())
        
        if path.exists('order_history.pickle'):
            with open('order_history.pickle', 'rb') as f:
                self.order_history = pickle.load(f)

    def get_ema_values(self):
        columns = self.data.columns
        ema_columns = []
        for ele in columns:
            if "-EMA" in ele:
                ema_columns.append(ele)
        ema_columns = list(dict.fromkeys(ema_columns))

        ema_nums = []
        for ele in ema_columns:
            tmp = ele[ele.find('-'):].replace('-EMA', '')
            ema_nums.append(int(tmp))
        ema_nums = list(dict.fromkeys(ema_nums))
        return ema_nums

    def plotGraph(self, coin=""):
        ema_nums = self.get_ema_values()
        for c in self.coinState:
            if not c.is_target_coin:
                continue
            
            if c.name != coin and coin != "":
                continue
        
            pl = self.data[c.name].plot(label=c.name)
            # self.data[f'{c.name}-MA'].plot(label=f'{c.name}-MA', ax=pl)
            for ema_num in ema_nums:
                self.data[f'{c.name}-EMA{ema_num}'].plot(label=f'{c.name}-EMA{ema_num}', ax=pl)

        plt.ylabel('Price')
        plt.legend()
        plt.show()

    def run(self):
        print(self.data)
        i = 0
        for c in self.coinState:
            if c.numHeld > 0.0:
                price = self.data.iloc[-1][c.name]
                currentValue = price * c.numHeld
                msg = f'Coin: {str(c.name)}\nHeld: {str(c.numHeld)}\nAmount bought: {str(c.numBought)}\nTime bought: {str(c.timeBought)}\nOrder ID: {str(c.lastBuyOrderID)}\nBought at: {str(c.purchasedPrice)} BTC\nCurrent value: {str(currentValue)} BTC\n'
                print(msg)

        print(self.order_history)
        self.plotGraph(coin="XMR")

e = SaveViewer()
e.run()