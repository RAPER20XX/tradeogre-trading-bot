## TODO

1. Better coin purchase handling
    - Better allowing coins to be bought multiple times
2. Use async http client instead of `requests`
3. Better predictions
    - Use machine learning for predicting future prices
        - I already have a framework on how to do this, but it's messy and old. It also doesn't support having muliple timesteps as output (for example giving it 100 previous data points and it returning 10 in the future)
4. Implement fancy discord webhook embeds with crypto logo and such
5. Move codebase over to rust (long term, not currently an issue)
6. Allowing to give the bot live commands through the terminal
7. General codebase cleanup (lot of the code is SUPER messy)