#!/bin/python
import datetime, pickle, sys, time

from datetime import datetime
from os.path import isfile, exists
from threading import Thread

import numpy as np
import pandas as pd
from dynaconf import Dynaconf

from libs.coin import coin
from libs.tradeogre import TradeOgre
from libs.utils import yaml_dump, RSI, stoch_signal
from libs.webhookmanager import WebhookManager
import traceback

class TradingBot:
    config_file = "config.yml"
    config = {"": ""}

    def initialize_fresh_config(self):
        print(f'Creating fresh config...')
        config = {
            "tokens": {
                "api": "Insert your API Key here",
                "secret": "Insert your Secret Key here"
            },
            "webhook": {
                "dowebhook": False,
                "webhookurl": "Insert your Webhook URL here",
            },
            "trading": {
                "strategy": {
                    "sellAboveBuyPricePercent": 0.0,
                    "buyBelowMAPercent": 3.0,
                    "buyIndexMin": 35.0,
                    "useEMA": True,
                    "useMA": False,
                    "useRSI": False,
                    "useFASTD": False
                },
                "periodWindow": 64,
                "movingAverageWindows": 32,
                "EMA1": 50,
                "EMA2": 200,
                "coins": ["XMR", "ETH", "LTC", "DOGE"],
                "panicSellPercent": 15,
                "onlySellProfitPercent": 2
            },
            "bot": {
                "UpdateInterval": 30.0,
                "printStateInterval": 4,
                "orderWaitTime": 3
            }
        }

        yaml_dump(config, self.config_file)
        print(f'Created new config. Read the README.md and configure the config at: {self.config_file}')
        exit()

    def verify_config(self, config):
        found_cfg_error = False
        if config.tokens.api in ["Insert your API Key here", ""]:
            print("ERROR: tokens.api must be a valid key")
            found_cfg_error = True

        if config.tokens.secret in ["Insert your Secret Key here", ""]:
            print("ERROR: tokens.secret must be a valid key")
            found_cfg_error = True

        if config.webhook.dowebhook and config.webhook.webhookurl in ["Insert your Webhook URL here", ""]:
            print("ERROR: webhook.webhookurl is invalid, but webhook.dowebhook is enabled")
            found_cfg_error = True

        if config.trading.periodWindow < 1:
            print("ERROR: trading.periodWindow must be above 1")
            found_cfg_error = True

        if config.trading.movingAverageWindows < 1:
            print("ERROR: trading.movingAverageWindows must be above 1")
            found_cfg_error = True

        if config.bot.UpdateInterval < 1:
            print("ERROR: bot.UpdateInterval must be above 1")
            found_cfg_error = True

        if len(config.trading.coins) == 0:
            self.output("ERROR: config.trading.coins is empty")
            found_cfg_error = True

        if config.trading.EMA1 > config.trading.EMA2:
            self.output("ERROR: EMA1 must be less than EMA2")
            found_cfg_error = True

        if found_cfg_error:
            sys.exit(1)

    def load_config(self):
        print(f'Loading config...')

        if not isfile(self.config_file):
            self.initialize_fresh_config()

        config = Dynaconf(settings_files=[self.config_file])

        self.verify_config(config)

        self.target_coins = []
        for coin in config.trading.coins:
            self.target_coins.append(coin.upper())

        self.sellAboveBuyPrice = config.trading.strategy.sellAboveBuyPricePercent / 100
        self.buyBelowMA = config.trading.strategy.buyBelowMAPercent / 100

        self.ema_nums.append(config.trading.EMA1)
        self.ema_nums.append(config.trading.EMA2)
        self.ema_nums = list(dict.fromkeys(self.ema_nums))

        self.config = config

    coinState = []

    COINS = []
    target_coins = []

    data = pd.DataFrame()

    lastHeartbeat = datetime.now()

    cache_display = [0.0]  # used to cache values and not print duplicates in `printState`

    order_history = pd.DataFrame()

    state = "Starting up"

    ema_nums = [5, 10, 50, 100, 200]
    def __init__(self):
        self.load_config()
        self.webhookmanager = WebhookManager(self.config.webhook.webhookurl)

        self.trade_ogre = TradeOgre(self.config.tokens.api, self.config.tokens.secret)
        self.COINS = self.trade_ogre.get_normalized_coins()

        self.loadState()

        valid_columns = []
        for c in self.target_coins:
            valid_columns.append(c)
            valid_columns.append(f'{c}-BID')
            valid_columns.append(f'{c}-LOW')
            valid_columns.append(f'{c}-HIGH')
            valid_columns.append(f'{c}-MA')
            valid_columns.append(f'{c}-RSI')
            valid_columns.append(f'{c}-fastd')
            for ema_num in self.ema_nums:
                valid_columns.append(f'{c}-EMA{str(ema_num)}')
            
        self.valid_columns = list(dict.fromkeys(valid_columns))
        self.data = self.loadDataframe()

        # Set up cache for printState``
        for _ in self.COINS:
            self.cache_display.append([0.0, 0.0, 0.0, "", 0.0, 0])

        self.max_data_len = max([self.config.trading.periodWindow, self.config.trading.movingAverageWindows, self.config.trading.EMA1, self.config.trading.EMA2])

        self.state = "Running"

    def shutdown(self):
        self.state = "Shutting down"
        self.output("Shutting down....")
        exit()

    def saveState(self):
        # save state
        with open('coinstate.pickle', 'wb') as f:
            pickle.dump(self.coinState, f)

        for column in self.data:
            if column not in self.valid_columns:
                del self.data[column]

        self.data.to_pickle('data.pickle')
        self.order_history.to_pickle('order_history.pickle')

    def loadState(self):
        if exists("coinstate.pickle"):
            # load state
            print("Saved state found, loading.")
            with open('coinstate.pickle', 'rb') as f:
                self.coinState = pickle.load(f)
        else:
            # create state storage object
            print("No state saved, starting from scratch.")
            self.coinState = []
            for c in self.COINS:
                is_target_coin = c in self.target_coins
                self.coinState.append(coin(c, is_target_coin))
        if exists('order_history.pickle'):
            with open('order_history.pickle', 'rb') as f:
                self.order_history = pickle.load(f)

    def loadDataframe(self):
        if exists("data.pickle"):
            print("Restoring state...")
            self.data = pd.read_pickle('data.pickle')
            for column in self.valid_columns:
                if column not in self.data.columns:
                    self.data[column] = np.nan
        else:
            self.data = pd.DataFrame(columns=self.valid_columns)

        return self.data

    def sell(self, coin, price, manualorder=False, waitfor=False):
        c = self.get_coin_index(coin)
        coinHeld = self.trade_ogre.get_bal(coin)

        if coinHeld <= 0.0:
            return

        if coinHeld * price < 0.00005:
            return

        profit = (coinHeld * price) - (coinHeld * self.coinState[c].purchasedPrice)

        print(f'Selling {str(coinHeld)} of {coin} at price {str(price)} BTC profit {str(profit)} BTC')

        sellresult = self.trade_ogre.sell_coin(coin, coinHeld, price)

        if not sellresult['success']:
            self.output(f'Could not sell: {sellresult}')
            return
        order_uuid = sellresult['uuid']

        if waitfor and order_uuid is not None:
            while True:
                order = self.trade_ogre.get_order(order_uuid)
                if order == 405:
                    break

                order_date = int(order['date']) 
                curr_time = int(time.time())
                time_left = curr_time - order_date

                if time_left >= self.config.bot.orderWaitTime:
                    self.trade_ogre.cancel_order(order_uuid)
                    self.output(f'Failed to buy {coin} in time, canceling')
                    return

                time.sleep(1)

        msg = ""
        if manualorder:
            msg += f'Manual Order!\n'
        percent_profit = ((price/self.coinState[c].purchasedPrice)*100)-100
        msg += f'LiveBot: Sold {str(coinHeld)} of {coin} at price {str(price)} BTC profit {str(profit)} BTC or {percent_profit}% profit'
        self.output(msg)

        trade_data = {
            "type": "sell",
            "price": price,
            "quantity": coinHeld,
            "market": self.trade_ogre.ticker_process(coin),
            "when": str(datetime.now()),
            "uuid": ''
        }

        self.order_history.append(trade_data, ignore_index=True)

        self.coinState[c].purchasedPrice = 0.0
        self.coinState[c].numHeld = 0.0
        self.coinState[c].numBought = 0.0
        self.coinState[c].lastBuyOrderID = ""
        self.coinState[c].timeBought = ""

    def buy(self, coin, price, manualorder=False, waitfor=False):
        c = self.get_coin_index(coin)
        availableBTC = self.trade_ogre.get_bal("BTC")

        if availableBTC < 0.00005:
            return

        if price < 0.00000001:
            print(f'Price is under 0.00000001 at {price}')
            return

        shares = (availableBTC - (availableBTC * 0.003)) / price
        if shares < 0.00005:
            return

        buyResult = self.trade_ogre.buy_coin(coin, shares, price)
        if buyResult['success']:
            order_uuid = buyResult['uuid']
            msg = ""
            if manualorder:
                msg += f'Manual Order!\n'
            msg += f'Submitted buy order for {shares} shares of {coin}'
            msg += str(buyResult)
            self.output(msg)
        else:
            self.output(f'Could not buy shares: {buyResult}')
            return

        if waitfor and order_uuid is not None:
            while True:
                order = self.trade_ogre.get_order(order_uuid)
                if order == 405:
                    break

                order_date = int(order['date']) 
                curr_time = int(time.time())
                time_left = curr_time - order_date

                if time_left >= self.config.bot.orderWaitTime:
                    self.trade_ogre.cancel_order(order_uuid)
                    self.output(f'Failed to buy {coin} in time, canceling')
                    return

                time.sleep(1)

        msg = f'TradeOgre Bot: I just purchased {str(shares)} shares of {coin} for {str(shares * price)} BTC. 1 {coin} is currently worth at {str(price)} BTC'
        self.output(msg)

        when = str(datetime.now())
        trade_data = {
            "type": "buy",
            "price": price,
            "quantity": shares,
            "market": self.trade_ogre.ticker_process(coin),
            "when": when,
            "uuid": self.coinState[c].lastBuyOrderID
        }

        self.order_history.append(trade_data, ignore_index=True)

        self.coinState[c].purchasedPrice = price
        self.coinState[c].lastBuyOrderID = order_uuid
        self.coinState[c].timeBought = when
        self.coinState[c].numHeld = shares
        self.coinState[c].numBought = shares

    def printState(self):
        total_value = 0.0
        i = 1
        for c in self.coinState:
            if c.name not in self.target_coins:
                continue
            
            if c.numHeld > 0.0:
                price = self.data.iloc[-1][c.name]
                currentValue = price * c.numHeld
                total_value += currentValue

                if self.cache_display[i][5] != self.config.bot.printStateInterval:
                    self.cache_display[i][5] += 1
                    continue
                
                if self.cache_display[i][0] != currentValue or self.cache_display[i][1] != c.numHeld or \
                        self.cache_display[i][2] != c.numBought or self.cache_display[i][3] != c.timeBought or \
                        self.cache_display[i][4] != c.purchasedPrice:
                    
                    msg = f'Coin: {str(c.name)}\nHeld: {str(c.numHeld)}\nAmount bought: {str(c.numBought)}\nTime bought: {str(c.timeBought)}\nOrder ID: {str(c.lastBuyOrderID)}\nBought at: {str(c.purchasedPrice)} BTC\nCurrent value: {str(currentValue)} BTC\n'
                    self.output(msg)

                    self.cache_display[i][0] = currentValue
                    self.cache_display[i][1] = c.numHeld
                    self.cache_display[i][2] = c.numBought
                    self.cache_display[i][3] = c.timeBought
                    self.cache_display[i][4] = c.purchasedPrice
                    self.cache_display[i][5] = 0
            i += 1

        total_value_2 = round(total_value + self.trade_ogre.get_bal('BTC'), 8)
        if self.cache_display[0] != total_value_2:
            self.output(f'Total value in BTC: {total_value_2}')
            self.cache_display[0] = total_value_2

    def checkSellCondition(self, coin, price):
        self.trade_ogre.ticker_check(coin)
        c = self.get_coin_index(coin)
        # look at values in last row only

        if self.coinState[c].numHeld <= 0.0:
            return False

        if self.coinState[c].purchasedPrice > price:
            return False

        if price > self.coinState[c].purchasedPrice * (1+(self.config.trading.onlySellProfitPercent/100)):
            return False

        if self.config.trading.strategy.useEMA and len(self.data) > self.config.trading.EMA2:
            EMA1 = self.data[f'{coin}-EMA{str(self.config.trading.EMA1)}'].tail(2).reset_index(drop=True)
            EMA2 = self.data[f'{coin}-EMA{str(self.config.trading.EMA2)}'].tail(2).reset_index(drop=True)
            if (EMA1[1] <= EMA2[1]) & (EMA1[0] >= EMA2[0]):
                self.output(f'Conditions met to sell {coin} via EMA')
                return True

        if self.coinState[c].purchasedPrice * (1-(self.config.trading.panicSellPercent/100)) >= price:
            self.output(f"Panic selling {coin}")
            return True

        if price > self.coinState[c].purchasedPrice + (self.coinState[c].purchasedPrice * self.sellAboveBuyPrice):
            self.output(f'Conditions met to sell {coin}')
            return True

        return False

    def checkBuyCondition(self, coin, price):
        self.trade_ogre.ticker_check(coin)
        c = self.get_coin_index(coin)

        # look at values in last row only
        movingAverage = self.data.iloc[-1][f'{coin}-MA']
        RSI = self.data.iloc[-1][f'{coin}-RSI']
        FASTD = self.data.iloc[-1][f'{coin}-fastd']

        if np.nan in [movingAverage, RSI, FASTD]:
            return False

        if self.coinState[c].numHeld != 0.0:
            return False

        if self.config.trading.strategy.useEMA and len(self.data) > self.config.trading.EMA2:
            EMA1 = self.data[f'{coin}-EMA{str(self.config.trading.EMA1)}'].tail(2).reset_index(drop=True)
            EMA2 = self.data[f'{coin}-EMA{str(self.config.trading.EMA2)}'].tail(2).reset_index(drop=True)
            do_sell = (EMA1[1] <= EMA2[1]) & (EMA1[0] >= EMA2[0])
            if ((EMA1[1] >= EMA2[1]) and (EMA1[0] <= EMA2[0])) and (not do_sell):
                return True

        if self.config.trading.strategy.useMA and (price < movingAverage - (movingAverage * self.buyBelowMA)):
            return True

        rsi_result = self.config.trading.strategy.buyIndexMin > RSI
        fastd_result = self.config.trading.strategy.buyIndexMin > FASTD

        if self.config.trading.strategy.useRSI and self.config.trading.strategy.useFASTD:
            if rsi_result and fastd_result:
                return True
        elif self.config.trading.strategy.useRSI:
            if fastd_result:
                return True
        elif self.config.trading.strategy.useFASTD:
            if rsi_result:
                return True

    def output(self, msg):
        date = datetime.now()
        year = date.year
        month = f"{date.month:02d}"
        day = f"{date.day:02d}"
        hour = f"{date.hour:02d}"
        minute = f"{date.minute:02d}"
        msg = f'[{year}-{month}-{day} {hour}:{minute}] {msg}'

        if self.config.webhook.dowebhook:
            self.webhookmanager.send_webhook(msg)

        print(msg)

    updating_data = False
    def update_data(self):
        self.updating_data = True
        currmarket = self.trade_ogre.get_whole_market()

        data_len = len(self.data)

        periodWindow = self.config.trading.periodWindow
        movingAverageWindows = self.config.trading.movingAverageWindows

        rowdata = {}
        for c in self.COINS:
            if c not in self.target_coins:
                continue
            currask = float(self.trade_ogre.get_coin_market_info(currmarket, c)['ask'])
            currbid = float(self.trade_ogre.get_coin_market_info(currmarket, c)['bid'])

            currhigh = float(self.trade_ogre.get_coin_market_info(currmarket, c)['high'])
            currlow = float(self.trade_ogre.get_coin_market_info(currmarket, c)['low'])

            rowdata.update({c: currask})
            rowdata.update({str(f'{c}-BID'): currbid})
            rowdata.update({str(f'{c}-HIGH'): currhigh})
            rowdata.update({str(f'{c}-LOW'): currlow})

        self.data = self.data.append(rowdata, ignore_index=True)

        for c in self.COINS:
            if c not in self.target_coins:
                continue

            self.data[f'{c}-MA'] = self.data[c].shift(1).rolling(window=movingAverageWindows).mean()
            self.data[f'{c}-RSI'] = RSI(self.data[c].values, timeperiod=periodWindow)

            self.data[f'{c}-fastd'] = stoch_signal(self.data[f'{c}-HIGH'], self.data[f'{c}-LOW'], self.data[c], timeperiod=periodWindow)

            for ema_num in self.ema_nums:
                ema_name = f'{c}-EMA{str(ema_num)}'
                if data_len >= self.config.trading.EMA1:
                    self.data[ema_name] = self.data[c].dropna().shift().fillna(self.data[ema_name]).ewm(com=ema_num).mean()
                else:
                    self.data[ema_name] = np.nan

        self.updating_data = False

    def get_coin_index(self, target):
        target = target.upper()
        return self.COINS.index(target)

    def buy_offmain_1(self, coin, price):
        self.buy(coin, price, waitfor=True)
    
    def sell_offmain_1(self, coin, price):
        self.sell(coin, price, waitfor=True)

    def sell_offmain(self, coin, price):
        print("sell_offmain")
        order_thread = Thread(target=self.sell_offmain_1, args=(coin, price))
        order_thread.daemon = True
        order_thread.start()
    
    def buy_offmain(self, coin, price):
        print("buy_offmain")
        order_thread = Thread(target=self.buy_offmain_1, args=(coin, price))
        order_thread.daemon = True
        order_thread.start()

    def run(self):
        self.output("Starting up...")
        self.output(f'len of data: {len(self.data)}')
        if self.max_data_len > len(self.data):
            self.output("Collecting data...")

        dataisbigenough = False

        while True:
            now = datetime.now()

            timeDiffHeartbeat = now - self.lastHeartbeat

            # report in not being dead every 16 minutes
            if timeDiffHeartbeat.total_seconds() > (60 * 16):
                msg = "Heartbeat"
                self.output(msg)
                self.lastHeartbeat = now

            if (not dataisbigenough) and len(self.data) >= self.max_data_len:
                self.output(f'Finished collecting data, len of data is now {self.max_data_len}')
                dataisbigenough = True

            self.update_data()

            for c in self.COINS:
                if c not in self.target_coins or (not dataisbigenough):
                    continue

                price = self.data.iloc[-1][c]
                if self.checkBuyCondition(c, price):
                    self.buy_offmain(c, price)
                    continue

                price = self.data.iloc[-1][f'{c}-BID']
                if self.checkSellCondition(c, price):
                    self.sell_offmain(c, price)

            self.saveState()
            self.printState()
            
            time.sleep(self.config.bot.UpdateInterval)


bot = TradingBot()
# bot.run()
try:
    bot.run()
except:
    traceback.print_exc()
finally:
    bot.shutdown()