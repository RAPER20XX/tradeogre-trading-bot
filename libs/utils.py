import json
import pandas as pd
import numpy as np
import math
import yaml
import datetime

def json_dump(data, file, indent=4):
    with open(file, 'w') as f:
        json.dump(data, f, indent=indent)

def json_load(file):
    with open(file) as f:
        data = json.load(f)
    return data

def yaml_dump(data, file, default_flow_style=False):
    with open(file, "w") as f:
        yaml.dump(data, file, default_flow_style=default_flow_style)

def yaml_load(file):
    with open(file) as f:
        data = yaml.safe_load(f)
    return data

def window_calc(targetwindow, currwindow, minwindow=0):
    if currwindow > targetwindow:
        return targetwindow
    elif currwindow > minwindow and minwindow != 0:
        return currwindow
    else:
        return targetwindow

def RSI(series, timeperiod=14):
    if timeperiod + 1 > len(series):
        return np.nan

    series = pd.DataFrame(series)
    delta = series.diff().dropna()
    u = delta * 0
    d = u.copy()
    u[delta > 0] = delta[delta > 0]
    d[delta < 0] = -delta[delta < 0]
    u[u.index[timeperiod - 1]] = np.mean(u[:timeperiod])  # first value is sum of avg gains
    u = u.drop(u.index[:(timeperiod - 1)])
    d[d.index[timeperiod - 1]] = np.mean(d[:timeperiod])  # first value is sum of avg losses
    d = d.drop(d.index[:(timeperiod - 1)])
    rs = pd.DataFrame.ewm(u, com=timeperiod - 1, adjust=False).mean() / \
         pd.DataFrame.ewm(d, com=timeperiod - 1, adjust=False).mean()
    return 100 - 100 / (1 + rs)

# Based off of https://github.com/bukosabino/ta
def stoch_signal(high, low, close, timeperiod=14):
    smooth_window = 3
    min_periods = 0
    smin = low.rolling(timeperiod, min_periods=min_periods).min()
    smax = high.rolling(timeperiod, min_periods=min_periods).max()
    stoch_k = 100 * (close - smin) / (smax - smin)

    stoch_d = stoch_k.rolling(
        smooth_window, min_periods=min_periods
    ).mean()
    return pd.Series(stoch_d, name="stoch_k_signal")

def roundDown(x, a):
    # rounds down x to the nearest multiple of a
    return math.floor(x / a) * a

def sats(amount, price, coin):
    sats = str(price * 100000000)
    sats = sats.split('.')[0] if '.' in sats else sats

    total = format(amount * price, '.8f')

    timestamp_print(f'[!] Buy placed for {amount} {coin} at {sats} satoshis, total of {total} BTC')

def timestamp_print(msg):
	date = datetime.datetime.now()
	year = date.year
	month = f"{date.month:02d}"
	day = f"{date.day:02d}"
	hour = f"{date.hour:02d}"
	minute = f"{date.minute:02d}"

	print(f'\u001b[36m[ {year}{month}{day} {hour}:{minute} ]\u001b[37m {msg}')