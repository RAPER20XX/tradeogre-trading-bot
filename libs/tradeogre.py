import json
import sys
import httpx as requests
import datetime
from libs.utils import timestamp_print

base_url = 'https://tradeogre.com/api/v1'

class TradeOgre:
	def __init__(self, trade_ogre_api, trade_ogre_secret):
		self.trade_ogre_secret = trade_ogre_secret
		self.trade_ogre_api = trade_ogre_api
		self.valid_tickers = self.get_all_tickers()

	def ticker_check(self, ticker, ticker_check=True):
		if ticker_check:
			ticker = self.ticker_process(ticker)
			
		if ticker not in self.valid_tickers:
			print(f'ERROR: invalid ticker: {ticker}')
			sys.exit(1)

	def ticker_process(self, coin):
		ticker = coin
		if "-" not in coin:
			ticker = f'BTC-{coin}'
		self.ticker_check(ticker, ticker_check=False)
		return ticker

	def get_all_tickers(self):
		whole_market = self.get_whole_market()
		tickers = []
		for ele in whole_market:
			ele = str(ele.keys())
			ticker = ele.replace("""'""", '').replace('])', '').replace('([', '').replace('dict_keys', '')
			tickers.append(ticker)
		return tickers

	def get_normalized_coins(self):
		tickers = self.get_all_tickers()
		normalized_coins = []
		for ticker in tickers:
			if ticker.startswith("BTC-"):
				normalized_coins.append(ticker.replace('BTC-', ''))
		return normalized_coins

	def get_coin_market_info(self, j, coin):
		ticker = self.ticker_process(coin)
		for i in j:
			if [k for k in i.keys()][0] == ticker:
				return i[ticker]
		print("ERROR: invalid ticker")
		sys.exit(1)

	def buy_coin(self, coin, amount, price):
		ticker = self.ticker_process(coin)
		api_response = self.buy_ticker(ticker, amount, price)
		return api_response


	def sell_coin(self, coin, amount, price):
		ticker = self.ticker_process(coin)
		api_response = self.sell_ticker(ticker, amount, price)
		return json.loads(api_response.content.decode('utf-8'))

	def get_bal(self, currency_ticker):
		balances = self.get_bal_raw()
		return float(balances[currency_ticker])

	def get_coins_in_bal(self):
		balances = self.get_bal_raw()
		all_coins = []
		for ele in balances:
			all_coins.append(ele)
		
		coins = []
		for coin in all_coins:
			bal = float(balances.get(coin))
			if bal != 0.0:
				coins.append(coin)
		
		return coins

	def get_whole_market(self):
		api_response = requests.get(f'{base_url}/markets', headers={'Content-Type': 'application/json', 'Authorization': f'Bearer {self.trade_ogre_secret}'})
		j = json.loads(api_response.content.decode('utf-8'))

		if api_response.status_code == 200:
			return j
		else:
			return None

	def get_market_info(self, coin):
		api_response = requests.get(f'{base_url}/markets', headers={'Content-Type': 'application/json', 'Authorization': f'Bearer {self.trade_ogre_secret}'})
		j = json.loads(api_response.content.decode('utf-8'))

		if api_response.status_code == 200:
			return self.get_coin_market_info(j, coin)
		else:
			return None

	def buy_ticker(self, ticker, amount, price):
		data = {"market": ticker, "quantity": amount, "price": price}
		response_api = requests.post(f'{base_url}/order/buy', data=data, auth=(self.trade_ogre_api, self.trade_ogre_secret)).json()
		if (not response_api['success']) and response_api['error'] == "Price must be at least 0.00000001":
			print(f'Tradeogre is not working again with prices!: {price}')
		
		return response_api

	def sell_ticker(self, ticker, amount, price):
		data = {"market": ticker, "quantity": amount, "price": price}

		return requests.post(f'{base_url}/order/sell', data=data, auth=(self.trade_ogre_api, self.trade_ogre_secret))


	def cancel_order(self, uuid):
		api_response = requests.post(f'{base_url}/order/cancel', data={'uuid':uuid}, auth=(self.trade_ogre_api, self.trade_ogre_secret))
		timestamp_print(f'[!] Order cancelled for {uuid}')
		return json.loads(api_response.content.decode('utf-8'))

	def get_bal_raw(self):
		api_response = requests.get(f'{base_url}/account/balances', auth=(self.trade_ogre_api, self.trade_ogre_secret))
		output = json.loads(api_response.content.decode('utf-8'))
		if not output['success']:
			print(f"ERROR: `get_bal_raw` failed to get balances with error: {output}")
			sys.exit(1)

		return output['balances']


	def order_book(self, market):
		ticker = self.ticker_process(market)
		api_response = requests.post(f'{base_url}/orders/{ticker}', auth=(self.trade_ogre_api, self.trade_ogre_secret))
		data = json.loads(api_response.content.decode('utf-8'))
		return data
	
	def get_all_orders(self):
		api_response = requests.post(f'{base_url}/account/orders', auth=(self.trade_ogre_api, self.trade_ogre_secret))
		data = json.loads(api_response.content.decode('utf-8'))
		return data
	
	def get_order(self, uuid):
		api_response = requests.post(f'{base_url}/account/order/{uuid}', auth=(self.trade_ogre_api, self.trade_ogre_secret))
		data = json.loads(api_response.content.decode('utf-8'))
		return data