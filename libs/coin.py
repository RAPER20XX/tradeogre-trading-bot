import sys


class coin:
    purchasedPrice = 0.0
    numHeld = 0.0
    numBought = 0.0
    name = ""
    lastBuyOrder = ""
    timeBought = ""
    is_target_coin = False

    def __str__(self):
        return f'name: {self.name} purchasedPrice: {self.purchasedPrice} numHeld: {self.numHeld} numBought: {self.numBought} lastBuyOrder: {self.lastBuyOrder} timeBought: {self.timeBought}'

    def __init__(self, name, is_target_coin):
        if is_target_coin not in [True, False]:
            print("ERROR: 'is_target_coin' MUST be boolean")
            sys.exit(1)

        self.name = name
        self.purchasedPrice = 0.0
        self.numHeld = 0.0
        self.numBought = 0.0
        self.timeBought = ""
        self.lastBuyOrderID = ""
        self.is_target_coin = is_target_coin
